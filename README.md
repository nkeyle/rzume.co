
**rzume.co is a Fast, Clean, Free Resume Builder.**

It uses *Browserify, React, jsx, Pubsub, Firebase, JQuery, Google auth and Open Sans.*

This bitbucket is more of a run and dump kind of repository, but feel free to contribute.

On my todo list...

* Skills list as tags
* line breaks within textarea (PRE)
* browserify compression prior to deploy
* dynamic page breaks based on content
* responsive design
* user editable sections / templates

----------

Release under the MIT License.

Copyright (c) 2016 Endice Software Pty Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.