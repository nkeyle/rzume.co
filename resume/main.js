"use strict";

(function () {

    // TODO Skills list as tags
    // TODO line breaks within textarea
    // TODO browserify thing
    // TODO dynamic page breaks
    // TODO responsive
    // TODO flexible sections / templates?

    var model_t = {
        name: "",
        title: "",
        bio: "",
        color1: "#1E90FF",
        color2: "#D34052",
        loc: "",
        email: "",
        twitter: "",
        phone: "",
        positions: [],
        featuredWorks: [],
        skills: [],
        educations: [],
        keyColor: "",
        toneColor: ""
    };

    model = model_t;

    var App = {
        addToCollection(collection, item) {
            model[collection] = model[collection] ? model[collection].concat([item]) : [item];
            App.save();
        },
        updateCollection(collection, info) {
            model[collection] = model[collection].map((c) => (c.id == info.id) ? info : c);
            App.save();
        },
        removeInCollection(collection, info) {
            model[collection] = model[collection].filter((c) => (c.id != info.id));
            App.save();
        },
        save() {
            publish("/data/changed");
        }
    };

    // BIO ////////////////////////////////////////////////////////////////////////////////////////////

    var Bio = React.createClass({
        bioChanged(key, e) {
            model[key] = e.target.value;
            App.save();
            render();
        },
        clearColors() {
            model.color1 = model_t.color1;
            model.color2 = model_t.color2;
            App.save();
            render();
        },
        render() {
            return <div className="formsection">
                <label>Full Name</label>
                <input value={this.props.name} onChange={this.bioChanged.bind(this, "name")} placeholder="Full Name"/>
                <label>Typical Job Title</label>
                <input value={this.props.title} onChange={this.bioChanged.bind(this, "title")} placeholder="Typical Job Title"/>
                <label>City, Country</label>
                <input value={this.props.loc} onChange={this.bioChanged.bind(this, "loc")} placeholder="City, Country"/>
                <label>Email Address</label>
                <input value={this.props.email} onChange={this.bioChanged.bind(this, "email")} placeholder="Email Address"/>
                <label>Twitter Handle</label>
                <input value={this.props.twitter} onChange={this.bioChanged.bind(this, "twitter")} placeholder="Twitter Handle"/>
                <label>Phone</label>
                <input value={this.props.phone} onChange={this.bioChanged.bind(this, "phone")} placeholder="Phone"/>
                <label>Mini Bio / Statement</label>
                <textarea value={this.props.bio} onChange={this.bioChanged.bind(this, "bio")} placeholder="Mini Bio / Statement"></textarea>
                <label>Colors</label>
                <button className="x" onClick={this.clearColors}>&#10005;</button>
                <input id="color1" type="color" className="colorpicker" value={this.props.color1}
                       onChange={this.bioChanged.bind(this, "color1")}></input>
                <input id="color2" type="color" className="colorpicker" value={this.props.color2}
                       onChange={this.bioChanged.bind(this, "color2")}></input>
            </div>;
        }
    });

    // POSITIONS ////////////////////////////////////////////////////////////////////////////////////////

    var Position = React.createClass({
        positionChanged(key, e) {
            var pos = {id: this.props.id, title: this.props.title, place: this.props.place, loc: this.props.loc, dates: this.props.dates};
            pos[key] = e.target.value;
            App.updateCollection("positions", pos);
            render();
        },
        remove() {
            App.removeInCollection("positions", {id: this.props.id});
            render();
        },
        render() {
            return (<div className="formitem">
                <button className="x" onClick={this.remove}>&#10005;</button>
                <button className="drag">&#9776;</button>
                <label>Position Title</label>
                <input value={this.props.title} onChange={this.positionChanged.bind(this, "title")} placeholder="Position Title"/>
                <label>Company</label>
                <input value={this.props.place} onChange={this.positionChanged.bind(this, "place")} placeholder="Company / Work Place"/>
                <label>Location</label>
                <input value={this.props.loc} onChange={this.positionChanged.bind(this, "loc")} placeholder="Location (City, Country)"/>
                <label>From - To Dates</label>
                <input value={this.props.dates} onChange={this.positionChanged.bind(this, "dates")} placeholder="From - To Dates"/>
            </div>);
        }
    });

    var PositionList = React.createClass({
        addPosition() {
            App.addToCollection("positions", {
                id: Date.now(), title: "", place: "", loc: "", dates: ""
            });
            render();
        },
        componentDidMount() {
            new Sortable(document.getElementById("positionList"), {
                handle: ".drag",
                onEnd: App.reorder.bind(this, "positions")
            });
        },
        render() {
            var comps = this.props.collection ?
                this.props.collection.map((e) =>
                    <Position title={e.title}
                              place={e.place}
                              loc={e.loc}
                              dates={e.dates}
                              id={e.id}
                              key={e.id}/>) : null;
            return <div className="formsection">
                <div id="positionList">{comps}</div>
                <button onClick={this.addPosition}>Add Position</button>
            </div>;
        }
    });

    // FEATURED WORKS ///////////////////////////////////////////////////////////////////////////////////////////

    var Feature = React.createClass({
        featureChanged(key, e) {
            var feat = {id: this.props.id, title: this.props.title, desc: this.props.desc};
            feat[key] = e.target.value;
            App.updateCollection("featuredWorks", feat);
            render();
        },
        remove() {
            App.removeInCollection("featuredWorks", {id: this.props.id});
            render();
        },
        render() {
            return (<div className="formitem">
                <button className="x" onClick={this.remove}>&#10005;</button>
                <button className="drag">&#9776;</button>
                <label>Title</label>
                <input value={this.props.title} onChange={this.featureChanged.bind(this, "title")} placeholder="Featured Work Title"/>
                <label>Description</label>
                <textarea value={this.props.desc} onChange={this.featureChanged.bind(this, "desc")} placeholder="Featured Work Description"/>
            </div>);
        }
    });

    var FeatureList = React.createClass({
        addFeature() {
            App.addToCollection("featuredWorks", {id: Date.now(), title: "", desc: ""});
            render();
        },
        componentDidMount() {
            new Sortable(document.getElementById("featureList"), {
                handle: ".drag",
                onEnd: App.reorder.bind(this, "featuredWorks")
            });
        },
        render() {
            var comps = this.props.collection ?
                this.props.collection.map((e) =>
                    <Feature title={e.title}
                             desc={e.desc}
                             id={e.id}
                             key={e.id}/>) : null;
            return <div className="formsection">
                <div id="featureList">{comps}</div>
                <button onClick={this.addFeature}>Add Featured Work</button>
            </div>;
        }
    });

    // SKILLS ///////////////////////////////////////////////////////////////////////////////////////////

    var Skill = React.createClass({
        skillChanged(key, e) {
            var skill = {id: this.props.id, level: this.props.level, items: this.props.items};
            skill[key] = e.target.value;
            App.updateCollection("skills", skill);
            render();
        },
        remove() {
            App.removeInCollection("skills", {id: this.props.id});
            render();
        },
        render() {
            return (<div className="formitem">
                <button className="x" onClick={this.remove}>&#10005;</button>
                <button className="drag">&#9776;</button>
                <label>Skill Level</label>
                <input value={this.props.level} onChange={this.skillChanged.bind(this, "level")} placeholder="Skill Level"/>
                <label>Items (use `-`)</label>
                <textarea value={this.props.items} onChange={this.skillChanged.bind(this, "items")}
                          placeholder="Microsoft Word - Microsoft Excel - etc."/>
            </div>);
        }
    });

    var SkillList = React.createClass({
        addSkill() {
            App.addToCollection("skills", {id: Date.now(), level: "", items: ""});
            render();
        },
        componentDidMount() {
            new Sortable(document.getElementById("skillList"), {
                handle: ".drag",
                onEnd: App.reorder.bind(this, "skills")
            });
        },
        render() {
            var comps = this.props.collection ?
                this.props.collection.map((e) =>
                    <Skill level={e.level}
                           items={e.items}
                           id={e.id}
                           key={e.id}/>) : null;
            return <div className="formsection">
                <div id="skillList">{comps}</div>
                <button onClick={this.addSkill} className="addButton">Add Skill Category</button>
            </div>;
        }
    });


    // EDUCATION ///////////////////////////////////////////////////////////////////////////////////////////

    var Education = React.createClass({
        changed(key, e) {
            var edu = {id: this.props.id, degree: this.props.degree, place: this.props.place, loc: this.props.loc, dates: this.props.dates};
            edu[key] = e.target.value;
            App.updateCollection("educations", edu);
            render();
        },
        remove() {
            App.removeInCollection("educations", {id: this.props.id});
            render();
        },
        render() {
            return (<div className="formitem">
                <button className="x" onClick={this.remove}>&#10005;</button>
                <button className="drag">&#9776;</button>
                <label>Degree Title</label>
                <input value={this.props.degree} onChange={this.changed.bind(this, "degree")} placeholder="Degree Title"/>
                <label>Place of Education</label>
                <input value={this.props.place} onChange={this.changed.bind(this, "place")} placeholder="Place of Education"/>
                <label>Location</label>
                <input value={this.props.loc} onChange={this.changed.bind(this, "loc")} placeholder="Location (City, Country)"/>
                <label>From - To Dates</label>
                <input value={this.props.dates} onChange={this.changed.bind(this, "dates")} placeholder="From - To Dates"/>
            </div>);
        }
    });

    var EducationList = React.createClass({
        add() {
            App.addToCollection("educations", {id: Date.now(), degree: "", place: "", loc: "", dates: ""});
            render();
        },
        componentDidMount() {
            new Sortable(document.getElementById("educationList"), {
                handle: ".drag",
                onEnd: App.reorder.bind(this, "educations")
            });
        },
        render() {
            var comps = this.props.collection ?
                this.props.collection.map((e) =>
                    <Education degree={e.degree}
                               place={e.place}
                               loc={e.loc}
                               dates={e.dates}
                               id={e.id}
                               key={e.id}/>) : null;
            return <div className="formsection">
                <div id="educationList">{comps}</div>
                <button onClick={this.add}>Add Education</button>
            </div>;
        }
    });

    /////////////////////////////////////////////////////////////////////////////////////////////

    var Main = React.createClass({
        onBioChanged(key, value) {
            model[key] = value;
            App.save();
            render();
        },
        render() {

            return (<div className="form">
                <Bio name={this.props.model.name}
                     bio={this.props.model.bio}
                     title={this.props.model.title}
                     loc={this.props.model.loc}
                     email={this.props.model.email}
                     twitter={this.props.model.twitter}
                     phone={this.props.model.phone}
                     color1={this.props.model.color1}
                     color2={this.props.model.color2}/>

                <b>Positions / Professional Experience</b>
                <PositionList collection={this.props.model.positions}/>

                <b>Featured Work</b>
                <FeatureList collection={this.props.model.featuredWorks}/>

                <b>Skills</b>
                <SkillList collection={this.props.model.skills}/>

                <b>Education / Training</b>
                <EducationList collection={this.props.model.educations}/>
            </div>);
        }
    });

    App.reorder = function (collection, e) {
        var item = model[collection][e.oldIndex];
        model[collection].splice(e.oldIndex, 1);
        model[collection].splice(e.newIndex, 0, item);
        App.save();
    };

    var render = function () {
        ReactDOM.render(
            <Main model={model}/>,
            document.getElementById("Main")
        );
    };

    subscribe("/data/loaded", function () {
        if (!model)
            model = model_t;
        if (!model.color1)
            model.color1 = model_t.color1;
        if (!model.color2)
            model.color2 = model_t.color2;

        render();
    });

})();
