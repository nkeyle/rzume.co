"use strict";

(function () {

    var Display = React.createClass({
        componentDidMount() {
            var m = this.props.model;
            if (!m)
                return null;
            $("strong").css("color", m.color1);
            $("h2").css("color", m.color2);
        },
        render() {
            var m = this.props.model;

            if (!m || !m.name)
                return <div className="Resume"><img className="instructions" src="../img/start.png" alt="instructions"/></div>;

            var mailto = "mailto:" + m.email;
            var twittlink = "";
            if (m.twitter) {
                var tweetto = "http://twitter.com/" + m.twitter;
                twittlink = <a href={tweetto} target='_blank'>@{m.twitter}</a>;
            }
            $("strong").css("color", m.color1);
            $("h2").css("color", m.color2);
            var bio = <div className="bio">
                <h1>{m.name}</h1>
                <section>
                    <h4>{m.title}</h4>

                    <h5>{m.loc}</h5>

                    <p>{m.bio}</p>
                </section>
                <aside>{m.phone}<br/><a href={mailto}>{m.email}</a><br/>{twittlink}</aside>
            </div>;

            var positionList = this.props.model.positions ?
                this.props.model.positions.map((e) =>
                    <div key={e.id} className="position">
                        <section>
                            <h3>{e.title}</h3>

                            <h2>{e.place}</h2>
                        </section>
                        <aside>{e.loc}<br />{e.dates}</aside>
                    </div>) : null;

            var featureList = this.props.model.featuredWorks ?
                this.props.model.featuredWorks.map((e) =>
                    <div key={e.id} className="feature">
                        <h2>{e.title}</h2>

                        <p>{e.desc}</p>
                    </div>) : null;

            var skillList = this.props.model.skills ?
                this.props.model.skills.map(function (e) {
                    var tags = e.items.split("-");
                    // TODO tags
                    return <div key={e.id} className="skill">
                        <h2>{e.level}</h2>

                        <p>{e.items}</p>
                    </div>;
                }) : null;

            var educationList = this.props.model.educations ?
                this.props.model.educations.map((e) =>
                    <div key={e.id} className="education">
                        <section>
                            <div>{e.degree}</div>
                            <h3>{e.place}</h3>
                        </section>
                        <aside>{e.loc}<br />{e.dates}</aside>
                    </div>) : null;

            var sig = <div className="signature">
                <div>{m.name}</div>
                <div>{twittlink}</div>
                <div><a href={mailto}>{m.email}</a></div>
                <aside>{m.phone}</aside>
            </div>;

            return (<div className="Resume">
                {bio}

                <strong>Professional Experience</strong>
                {positionList}

                <strong>Featured Work</strong>
                {featureList}

                <strong>Skills</strong>
                {skillList}

                <strong>Education / Training</strong>
                {educationList}

                {sig}
            </div>);
        }
    });

    var render = function () {
        ReactDOM.render(
            <Display model={model}/>,
            document.getElementById("Display")
        );
    };

    subscribe("/data/loaded", render);
    subscribe("/data/changed", render);

    subscribe("/user/signedin", function (user) {
        if (!user || !user.hasOwnProperty("photoURL") || !user.photoURL)
            return;
        $('#profilePic').attr("src", user.photoURL);
        $('#profilePic').css("visibility", "visible");
    });
})();
