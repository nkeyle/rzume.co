(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

(function () {

    var Display = React.createClass({
        displayName: "Display",

        componentDidMount() {
            var m = this.props.model;
            if (!m) return null;
            $("strong").css("color", m.color1);
            $("h2").css("color", m.color2);
        },
        render() {
            var m = this.props.model;

            if (!m || !m.name) return React.createElement(
                "div",
                { className: "Resume" },
                React.createElement("img", { className: "instructions", src: "../img/start.png", alt: "instructions" })
            );

            var mailto = "mailto:" + m.email;
            var twittlink = "";
            if (m.twitter) {
                var tweetto = "http://twitter.com/" + m.twitter;
                twittlink = React.createElement(
                    "a",
                    { href: tweetto, target: "_blank" },
                    "@",
                    m.twitter
                );
            }
            $("strong").css("color", m.color1);
            $("h2").css("color", m.color2);
            var bio = React.createElement(
                "div",
                { className: "bio" },
                React.createElement(
                    "h1",
                    null,
                    m.name
                ),
                React.createElement(
                    "section",
                    null,
                    React.createElement(
                        "h4",
                        null,
                        m.title
                    ),
                    React.createElement(
                        "h5",
                        null,
                        m.loc
                    ),
                    React.createElement(
                        "p",
                        null,
                        m.bio
                    )
                ),
                React.createElement(
                    "aside",
                    null,
                    m.phone,
                    React.createElement("br", null),
                    React.createElement(
                        "a",
                        { href: mailto },
                        m.email
                    ),
                    React.createElement("br", null),
                    twittlink
                )
            );

            var positionList = this.props.model.positions ? this.props.model.positions.map(e => React.createElement(
                "div",
                { key: e.id, className: "position" },
                React.createElement(
                    "section",
                    null,
                    React.createElement(
                        "h3",
                        null,
                        e.title
                    ),
                    React.createElement(
                        "h2",
                        null,
                        e.place
                    )
                ),
                React.createElement(
                    "aside",
                    null,
                    e.loc,
                    React.createElement("br", null),
                    e.dates
                )
            )) : null;

            var featureList = this.props.model.featuredWorks ? this.props.model.featuredWorks.map(e => React.createElement(
                "div",
                { key: e.id, className: "feature" },
                React.createElement(
                    "h2",
                    null,
                    e.title
                ),
                React.createElement(
                    "p",
                    null,
                    e.desc
                )
            )) : null;

            var skillList = this.props.model.skills ? this.props.model.skills.map(function (e) {
                var tags = e.items.split("-");
                // TODO tags
                return React.createElement(
                    "div",
                    { key: e.id, className: "skill" },
                    React.createElement(
                        "h2",
                        null,
                        e.level
                    ),
                    React.createElement(
                        "p",
                        null,
                        e.items
                    )
                );
            }) : null;

            var educationList = this.props.model.educations ? this.props.model.educations.map(e => React.createElement(
                "div",
                { key: e.id, className: "education" },
                React.createElement(
                    "section",
                    null,
                    React.createElement(
                        "div",
                        null,
                        e.degree
                    ),
                    React.createElement(
                        "h3",
                        null,
                        e.place
                    )
                ),
                React.createElement(
                    "aside",
                    null,
                    e.loc,
                    React.createElement("br", null),
                    e.dates
                )
            )) : null;

            var sig = React.createElement(
                "div",
                { className: "signature" },
                React.createElement(
                    "div",
                    null,
                    m.name
                ),
                React.createElement(
                    "div",
                    null,
                    twittlink
                ),
                React.createElement(
                    "div",
                    null,
                    React.createElement(
                        "a",
                        { href: mailto },
                        m.email
                    )
                ),
                React.createElement(
                    "aside",
                    null,
                    m.phone
                )
            );

            return React.createElement(
                "div",
                { className: "Resume" },
                bio,
                React.createElement(
                    "strong",
                    null,
                    "Professional Experience"
                ),
                positionList,
                React.createElement(
                    "strong",
                    null,
                    "Featured Work"
                ),
                featureList,
                React.createElement(
                    "strong",
                    null,
                    "Skills"
                ),
                skillList,
                React.createElement(
                    "strong",
                    null,
                    "Education / Training"
                ),
                educationList,
                sig
            );
        }
    });

    var render = function () {
        ReactDOM.render(React.createElement(Display, { model: model }), document.getElementById("Display"));
    };

    subscribe("/data/loaded", render);
    subscribe("/data/changed", render);

    subscribe("/user/signedin", function (user) {
        if (!user || !user.hasOwnProperty("photoURL") || !user.photoURL) return;
        $('#profilePic').attr("src", user.photoURL);
        $('#profilePic').css("visibility", "visible");
    });
})();

},{}]},{},[1]);
