"use strict";

var Store = Store || {};

document.addEventListener('DOMContentLoaded', function () {
    (function (Store) {

        Store.uid = null;
        var tid = null;

        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {
                console.log("user signed in ");
                Store.uid = user.uid;
                Store.load();
                publish("/user/signedin", [user]);
            } else {
                console.log("user NOT signed in ");
                window.location = "/";
            }
        });

        Store.signout = function () {
            firebase.auth().signOut().then(function () {
                window.location = "/";
            }, function (error) {
                console.log("error signing out");
                console.log(error);
            });
        };

        Store.signin = function () {
            var provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().signInWithPopup(provider).then(function (result) {
                // loggedin
            }).catch(function (error) {
                console.log("Error login");
                console.log(error);
                debugger;
            });
        };

        Store.load = function () {
            firebase.database()
                .ref('/users/' + Store.uid)
                .once('value')
                .then(function (snapshot) {
                    model = snapshot.val();
                    console.log('data loaded');
                    publish("/data/loaded");
                }, function (error) {
                    console.log("error on load");
                    console.log(error);
                    publish("/data/loaded");
                });
        };

        Store.save = function () {
            if ($.isEmptyObject(model))
                return;
            if (!Store.uid)
                return;
            if (tid)
                clearTimeout(tid);
            tid = setTimeout(persist, 1001);
        };

        var persist = function () {
            firebase.database()
                .ref('users/' + Store.uid)
                .set(model)
                .then(function () {
                    console.log('data saved');
                }, function (error) {
                    console.log("error on save");
                    console.log(error);
                });
        };

        subscribe("/data/changed", Store.save);

        // CTRL+S Save
        $(document).bind('keydown', function (e) {
            if ((e.ctrlKey || e.metaKey) && (e.which == 83)) {
                e.preventDefault();
                if (tid)
                    clearTimeout(tid);
                persist();
                return false;
            }
        });

    })(Store);
});

