(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

(function () {

    // TODO Skills list as tags
    // TODO line breaks within textarea
    // TODO browserify thing
    // TODO dynamic page breaks
    // TODO responsive
    // TODO flexible sections / templates?

    var model_t = {
        name: "",
        title: "",
        bio: "",
        color1: "#1E90FF",
        color2: "#D34052",
        loc: "",
        email: "",
        twitter: "",
        phone: "",
        positions: [],
        featuredWorks: [],
        skills: [],
        educations: [],
        keyColor: "",
        toneColor: ""
    };

    model = model_t;

    var App = {
        addToCollection(collection, item) {
            model[collection] = model[collection] ? model[collection].concat([item]) : [item];
            App.save();
        },
        updateCollection(collection, info) {
            model[collection] = model[collection].map(c => c.id == info.id ? info : c);
            App.save();
        },
        removeInCollection(collection, info) {
            model[collection] = model[collection].filter(c => c.id != info.id);
            App.save();
        },
        save() {
            publish("/data/changed");
        }
    };

    // BIO ////////////////////////////////////////////////////////////////////////////////////////////

    var Bio = React.createClass({
        displayName: "Bio",

        bioChanged(key, e) {
            model[key] = e.target.value;
            App.save();
            render();
        },
        clearColors() {
            model.color1 = model_t.color1;
            model.color2 = model_t.color2;
            App.save();
            render();
        },
        render() {
            return React.createElement(
                "div",
                { className: "formsection" },
                React.createElement(
                    "label",
                    null,
                    "Full Name"
                ),
                React.createElement("input", { value: this.props.name, onChange: this.bioChanged.bind(this, "name"), placeholder: "Full Name" }),
                React.createElement(
                    "label",
                    null,
                    "Typical Job Title"
                ),
                React.createElement("input", { value: this.props.title, onChange: this.bioChanged.bind(this, "title"), placeholder: "Typical Job Title" }),
                React.createElement(
                    "label",
                    null,
                    "City, Country"
                ),
                React.createElement("input", { value: this.props.loc, onChange: this.bioChanged.bind(this, "loc"), placeholder: "City, Country" }),
                React.createElement(
                    "label",
                    null,
                    "Email Address"
                ),
                React.createElement("input", { value: this.props.email, onChange: this.bioChanged.bind(this, "email"), placeholder: "Email Address" }),
                React.createElement(
                    "label",
                    null,
                    "Twitter Handle"
                ),
                React.createElement("input", { value: this.props.twitter, onChange: this.bioChanged.bind(this, "twitter"), placeholder: "Twitter Handle" }),
                React.createElement(
                    "label",
                    null,
                    "Phone"
                ),
                React.createElement("input", { value: this.props.phone, onChange: this.bioChanged.bind(this, "phone"), placeholder: "Phone" }),
                React.createElement(
                    "label",
                    null,
                    "Mini Bio / Statement"
                ),
                React.createElement("textarea", { value: this.props.bio, onChange: this.bioChanged.bind(this, "bio"), placeholder: "Mini Bio / Statement" }),
                React.createElement(
                    "label",
                    null,
                    "Colors"
                ),
                React.createElement(
                    "button",
                    { className: "x", onClick: this.clearColors },
                    "✕"
                ),
                React.createElement("input", { id: "color1", type: "color", className: "colorpicker", value: this.props.color1,
                    onChange: this.bioChanged.bind(this, "color1") }),
                React.createElement("input", { id: "color2", type: "color", className: "colorpicker", value: this.props.color2,
                    onChange: this.bioChanged.bind(this, "color2") })
            );
        }
    });

    // POSITIONS ////////////////////////////////////////////////////////////////////////////////////////

    var Position = React.createClass({
        displayName: "Position",

        positionChanged(key, e) {
            var pos = { id: this.props.id, title: this.props.title, place: this.props.place, loc: this.props.loc, dates: this.props.dates };
            pos[key] = e.target.value;
            App.updateCollection("positions", pos);
            render();
        },
        remove() {
            App.removeInCollection("positions", { id: this.props.id });
            render();
        },
        render() {
            return React.createElement(
                "div",
                { className: "formitem" },
                React.createElement(
                    "button",
                    { className: "x", onClick: this.remove },
                    "✕"
                ),
                React.createElement(
                    "button",
                    { className: "drag" },
                    "☰"
                ),
                React.createElement(
                    "label",
                    null,
                    "Position Title"
                ),
                React.createElement("input", { value: this.props.title, onChange: this.positionChanged.bind(this, "title"), placeholder: "Position Title" }),
                React.createElement(
                    "label",
                    null,
                    "Company"
                ),
                React.createElement("input", { value: this.props.place, onChange: this.positionChanged.bind(this, "place"), placeholder: "Company / Work Place" }),
                React.createElement(
                    "label",
                    null,
                    "Location"
                ),
                React.createElement("input", { value: this.props.loc, onChange: this.positionChanged.bind(this, "loc"), placeholder: "Location (City, Country)" }),
                React.createElement(
                    "label",
                    null,
                    "From - To Dates"
                ),
                React.createElement("input", { value: this.props.dates, onChange: this.positionChanged.bind(this, "dates"), placeholder: "From - To Dates" })
            );
        }
    });

    var PositionList = React.createClass({
        displayName: "PositionList",

        addPosition() {
            App.addToCollection("positions", {
                id: Date.now(), title: "", place: "", loc: "", dates: ""
            });
            render();
        },
        componentDidMount() {
            new Sortable(document.getElementById("positionList"), {
                handle: ".drag",
                onEnd: App.reorder.bind(this, "positions")
            });
        },
        render() {
            var comps = this.props.collection ? this.props.collection.map(e => React.createElement(Position, { title: e.title,
                place: e.place,
                loc: e.loc,
                dates: e.dates,
                id: e.id,
                key: e.id })) : null;
            return React.createElement(
                "div",
                { className: "formsection" },
                React.createElement(
                    "div",
                    { id: "positionList" },
                    comps
                ),
                React.createElement(
                    "button",
                    { onClick: this.addPosition },
                    "Add Position"
                )
            );
        }
    });

    // FEATURED WORKS ///////////////////////////////////////////////////////////////////////////////////////////

    var Feature = React.createClass({
        displayName: "Feature",

        featureChanged(key, e) {
            var feat = { id: this.props.id, title: this.props.title, desc: this.props.desc };
            feat[key] = e.target.value;
            App.updateCollection("featuredWorks", feat);
            render();
        },
        remove() {
            App.removeInCollection("featuredWorks", { id: this.props.id });
            render();
        },
        render() {
            return React.createElement(
                "div",
                { className: "formitem" },
                React.createElement(
                    "button",
                    { className: "x", onClick: this.remove },
                    "✕"
                ),
                React.createElement(
                    "button",
                    { className: "drag" },
                    "☰"
                ),
                React.createElement(
                    "label",
                    null,
                    "Title"
                ),
                React.createElement("input", { value: this.props.title, onChange: this.featureChanged.bind(this, "title"), placeholder: "Featured Work Title" }),
                React.createElement(
                    "label",
                    null,
                    "Description"
                ),
                React.createElement("textarea", { value: this.props.desc, onChange: this.featureChanged.bind(this, "desc"), placeholder: "Featured Work Description" })
            );
        }
    });

    var FeatureList = React.createClass({
        displayName: "FeatureList",

        addFeature() {
            App.addToCollection("featuredWorks", { id: Date.now(), title: "", desc: "" });
            render();
        },
        componentDidMount() {
            new Sortable(document.getElementById("featureList"), {
                handle: ".drag",
                onEnd: App.reorder.bind(this, "featuredWorks")
            });
        },
        render() {
            var comps = this.props.collection ? this.props.collection.map(e => React.createElement(Feature, { title: e.title,
                desc: e.desc,
                id: e.id,
                key: e.id })) : null;
            return React.createElement(
                "div",
                { className: "formsection" },
                React.createElement(
                    "div",
                    { id: "featureList" },
                    comps
                ),
                React.createElement(
                    "button",
                    { onClick: this.addFeature },
                    "Add Featured Work"
                )
            );
        }
    });

    // SKILLS ///////////////////////////////////////////////////////////////////////////////////////////

    var Skill = React.createClass({
        displayName: "Skill",

        skillChanged(key, e) {
            var skill = { id: this.props.id, level: this.props.level, items: this.props.items };
            skill[key] = e.target.value;
            App.updateCollection("skills", skill);
            render();
        },
        remove() {
            App.removeInCollection("skills", { id: this.props.id });
            render();
        },
        render() {
            return React.createElement(
                "div",
                { className: "formitem" },
                React.createElement(
                    "button",
                    { className: "x", onClick: this.remove },
                    "✕"
                ),
                React.createElement(
                    "button",
                    { className: "drag" },
                    "☰"
                ),
                React.createElement(
                    "label",
                    null,
                    "Skill Level"
                ),
                React.createElement("input", { value: this.props.level, onChange: this.skillChanged.bind(this, "level"), placeholder: "Skill Level" }),
                React.createElement(
                    "label",
                    null,
                    "Items (use `-`)"
                ),
                React.createElement("textarea", { value: this.props.items, onChange: this.skillChanged.bind(this, "items"),
                    placeholder: "Microsoft Word - Microsoft Excel - etc." })
            );
        }
    });

    var SkillList = React.createClass({
        displayName: "SkillList",

        addSkill() {
            App.addToCollection("skills", { id: Date.now(), level: "", items: "" });
            render();
        },
        componentDidMount() {
            new Sortable(document.getElementById("skillList"), {
                handle: ".drag",
                onEnd: App.reorder.bind(this, "skills")
            });
        },
        render() {
            var comps = this.props.collection ? this.props.collection.map(e => React.createElement(Skill, { level: e.level,
                items: e.items,
                id: e.id,
                key: e.id })) : null;
            return React.createElement(
                "div",
                { className: "formsection" },
                React.createElement(
                    "div",
                    { id: "skillList" },
                    comps
                ),
                React.createElement(
                    "button",
                    { onClick: this.addSkill, className: "addButton" },
                    "Add Skill Category"
                )
            );
        }
    });

    // EDUCATION ///////////////////////////////////////////////////////////////////////////////////////////

    var Education = React.createClass({
        displayName: "Education",

        changed(key, e) {
            var edu = { id: this.props.id, degree: this.props.degree, place: this.props.place, loc: this.props.loc, dates: this.props.dates };
            edu[key] = e.target.value;
            App.updateCollection("educations", edu);
            render();
        },
        remove() {
            App.removeInCollection("educations", { id: this.props.id });
            render();
        },
        render() {
            return React.createElement(
                "div",
                { className: "formitem" },
                React.createElement(
                    "button",
                    { className: "x", onClick: this.remove },
                    "✕"
                ),
                React.createElement(
                    "button",
                    { className: "drag" },
                    "☰"
                ),
                React.createElement(
                    "label",
                    null,
                    "Degree Title"
                ),
                React.createElement("input", { value: this.props.degree, onChange: this.changed.bind(this, "degree"), placeholder: "Degree Title" }),
                React.createElement(
                    "label",
                    null,
                    "Place of Education"
                ),
                React.createElement("input", { value: this.props.place, onChange: this.changed.bind(this, "place"), placeholder: "Place of Education" }),
                React.createElement(
                    "label",
                    null,
                    "Location"
                ),
                React.createElement("input", { value: this.props.loc, onChange: this.changed.bind(this, "loc"), placeholder: "Location (City, Country)" }),
                React.createElement(
                    "label",
                    null,
                    "From - To Dates"
                ),
                React.createElement("input", { value: this.props.dates, onChange: this.changed.bind(this, "dates"), placeholder: "From - To Dates" })
            );
        }
    });

    var EducationList = React.createClass({
        displayName: "EducationList",

        add() {
            App.addToCollection("educations", { id: Date.now(), degree: "", place: "", loc: "", dates: "" });
            render();
        },
        componentDidMount() {
            new Sortable(document.getElementById("educationList"), {
                handle: ".drag",
                onEnd: App.reorder.bind(this, "educations")
            });
        },
        render() {
            var comps = this.props.collection ? this.props.collection.map(e => React.createElement(Education, { degree: e.degree,
                place: e.place,
                loc: e.loc,
                dates: e.dates,
                id: e.id,
                key: e.id })) : null;
            return React.createElement(
                "div",
                { className: "formsection" },
                React.createElement(
                    "div",
                    { id: "educationList" },
                    comps
                ),
                React.createElement(
                    "button",
                    { onClick: this.add },
                    "Add Education"
                )
            );
        }
    });

    /////////////////////////////////////////////////////////////////////////////////////////////

    var Main = React.createClass({
        displayName: "Main",

        onBioChanged(key, value) {
            model[key] = value;
            App.save();
            render();
        },
        render() {

            return React.createElement(
                "div",
                { className: "form" },
                React.createElement(Bio, { name: this.props.model.name,
                    bio: this.props.model.bio,
                    title: this.props.model.title,
                    loc: this.props.model.loc,
                    email: this.props.model.email,
                    twitter: this.props.model.twitter,
                    phone: this.props.model.phone,
                    color1: this.props.model.color1,
                    color2: this.props.model.color2 }),
                React.createElement(
                    "b",
                    null,
                    "Positions / Professional Experience"
                ),
                React.createElement(PositionList, { collection: this.props.model.positions }),
                React.createElement(
                    "b",
                    null,
                    "Featured Work"
                ),
                React.createElement(FeatureList, { collection: this.props.model.featuredWorks }),
                React.createElement(
                    "b",
                    null,
                    "Skills"
                ),
                React.createElement(SkillList, { collection: this.props.model.skills }),
                React.createElement(
                    "b",
                    null,
                    "Education / Training"
                ),
                React.createElement(EducationList, { collection: this.props.model.educations })
            );
        }
    });

    App.reorder = function (collection, e) {
        var item = model[collection][e.oldIndex];
        model[collection].splice(e.oldIndex, 1);
        model[collection].splice(e.newIndex, 0, item);
        App.save();
    };

    var render = function () {
        ReactDOM.render(React.createElement(Main, { model: model }), document.getElementById("Main"));
    };

    subscribe("/data/loaded", function () {
        if (!model) model = model_t;
        if (!model.color1) model.color1 = model_t.color1;
        if (!model.color2) model.color2 = model_t.color2;

        render();
    });
})();

},{}]},{},[1]);
