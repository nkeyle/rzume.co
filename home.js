"use strict";

var home = home || {};

(function (home) {

    firebase.initializeApp({
        apiKey: "AIzaSyAmT8KD3dkZXEM0igiEz_3iSCHILt4hqCg",
        authDomain: "project-1577416179939660492.firebaseapp.com",
        databaseURL: "https://project-1577416179939660492.firebaseio.com"
    });

    home.signin = function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            window.location = "/resume";
        }).catch(function (error) {
            console.log("Error login");
            console.log(error);
        });
        return false;
    }

})(home);
