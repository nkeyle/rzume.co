@ECHO OFF

cd resume\
call browserify main.js -o main.o.js -t [ babelify --presets [  react ] ]
call browserify display.jsx -o display.o.js -t [ babelify --presets [  react ] ]
cd ..

xcopy /y *.js web\
xcopy /y *.css web\
xcopy /y *.html web\
xcopy /y img\*.* web\img\*.*
xcopy /y extern\*.* web\extern\*.*
xcopy /y resume\*.* web\resume\*.*

echo.
echo.
echo Will need to update js links

cd web\resume
cd ..\extern
REM del browser.min.js

cd ..\..

pause
